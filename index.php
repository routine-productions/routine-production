<!DOCTYPE html>
<html>

<head lang="ru">
    <meta charset="UTF-8">
    <title>Routine-Production</title>

    <link
        href='https://fonts.googleapis.com/css?family=Roboto:900italic,900,700italic,700,500italic,500,400italic,400,300italic,300,100italic,100&subset=latin,cyrillic'
        rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="/css/index.min.css" type="text/css"/>
    <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>

<body>
<?php require __DIR__ . "/img/sprite.svg"; ?>

<?php

require_once __DIR__ . '/libs/Mobile_Detect.php';
$Detect = new Mobile_Detect;
$Dir_Images = __DIR__ . '/images/';
?>

<div class="Header-Wrap">
    <?php require_once __DIR__ . '/modules/header.php'; ?>
</div>
<main>
    <?php
    if ($_SERVER['REQUEST_URI'] == '/') {
        require __DIR__ . '/pages/technical-task.php';

    } else {
        require __DIR__ . '/pages/' . $_SERVER['REQUEST_URI'] . '.php';
    }
    ?>

</main>
<footer class="Site-Footer">
    <?php require_once __DIR__ . '/modules/footer.php'; ?>

</footer>


<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="index.min.js"></script>

<body>


</html>

