<section class="Task">
    <h2 class="Task-Head">Техническое задание на выполнение работы</h2>
    <ul class="Task-Nav">
        <li class="Link">1. Общая информация</li>
        <li class="Break"><span></span></li>
        <li class="Link">2. Структура</li>
        <li class="Break"><span></span></li>
        <li class="Link">3. Функционал</li>
        <li class="Break"><span></span></li>
        <li class="Link">4. Дизайн</li>
    </ul>

    <form class="Task-Form">

        <!--    STEP-1-GENERAL-->
        <div class="General">
            <div class="Name">
                <span>Название сайта</span><input placeholder="Example Shop" type="text"/>
            </div>
            <div class="Address">
                <span>Адрес сайта</span>
                <input placeholder="example-shop.ru" type="text"/>

                <div class="Additional">
                    <input id="by-address" type="checkbox"/>
                    <label for="by-address">Дополнительно требуется приобретение этого адреса</label>
                </div>
            </div>
            <div class="Type">
                <span>Тип сайта</span>

                <div class="Select">
                    <div>
                        <input type="text">

                        <div class="Dropdown">
                            <svg>
                                <use xlink:href="#arrow"></use>
                            </svg>
                        </div>
                    </div>
                </div>


            </div>
            <div class="Description">
            <span>
                Краткое описание
                деятельности
                компании
            </span>
            <textarea
                placeholder="Example Company занимается продажей компьютерных комплектующих в Москве и области. Мы ориентируемся на бюджетный сегмент. На рынке с 1998 года, имеем 6 сетевых магазинов в Москве и 3 в Московской области."></textarea>

            </div>
            <button class="Go-Next">Перейти к шагу 2</button>

        </div>
        <!--    STEP-2-STRUCTURE-->
        <div class="Structure">
            <!--        STRUCTURE-OF-ONE-PAGE-->
            <div class="One-Page">
                <div class="Name">
                    <span>Название страницы</span><input placeholder="Главная" type="text"/>
                </div>
                <div class="Description">
                <span>
                    Описание содержания
                </span>
                <textarea
                    placeholder="Слайдер с текущими акциями, разделы новых, популярных и акционных товаров."></textarea>

                </div>
                <button class="Delete-Page">
                    <svg>
                        <use xlink:href="#close"></use>
                    </svg>
                    Удалить страницу
                </button>
            </div>
            <div class="One-Page">
                <div class="Name">
                    <span>Название страницы</span>
                    <input placeholder="Контактная информация" type="text"/>
                </div>
                <div class="Description">
                <span>
                    Описание содержания
                </span>
                <textarea
                    placeholder="Карта проезда, адреса, телефоны, платёжные реквизиты и юридическая информация. Текст в приложении 3."></textarea>

                </div>
                <button class="Delete-Page">
                    <svg>
                        <use xlink:href="#close"></use>
                    </svg>
                    Удалить страницу
                </button>
            </div>
            <div class="Add-Page">
                <button><span>+</span>Добавить страницу</button>
            </div>
            <button class="Go-Next">Перейти к шагу 3</button>
        </div>
        <!--    STEP-3-FUNCTIONAL-->
        <div class="Functional">

            <div class="One-Page">
                <div class="Name">
                    <span>Название функционала</span><input placeholder="Акционный слайдер" type="text"/>
                </div>
                <div class="Description">
                <span>
                    Описание содержания
                </span>
                <textarea placeholder="Возможность добавления менеджером заголовка, короткого текста (до 500 символов), фона, изображения сопровождающего текст.
                Возможность выбора начального и конечного дня показа акции. Истекшие акции должны удаляться из слайдера в архив."></textarea>
                </div>
                <div class="Add-Page">
                    <button><span>+</span>Добавить страницу</button>
                </div>

                <div class="One-Page">
                    <div class="Name">
                        <span>Название функционала</span><input placeholder="Галерея фотографий товара"
                                                                type="text"/>
                    </div>
                    <div class="Description">
                <span>
                    Описание содержания
                </span>
                    <textarea
                        placeholder="Добавление фотографий менеджером. Просмотр увеличенного фото при наведении на  превью."></textarea>
                    </div>
                </div>
                <div class="Add-Page">
                    <button><span>+</span>Добавить страницу</button>
                </div>
                <button class="Go-Next">Перейти к шагу 4</button>


            </div>
        </div>
        <!--    STEP-4-STYLING-->
        <div class="Styling">
            <div class="Style">
                <span>Наличие фирменного стиля</span>

                <div class="Check-Wrap">
                    <div class="Check">
                        <button>Есть</button>
                        <button class="Active">Нет</button>
                    </div>
                </div>
                <div class="Add-Files">
                    <button><span>+</span>Добавить файлы</button>
                </div>
            </div>
            <div class="Style">
                <span>Наличие логотипа</span>

                <div class="Check-Wrap">
                    <div class="Check">
                        <button>Есть</button>
                        <button class="Active">Нет</button>
                    </div>
                </div>
                <div class="Add-Files">
                    <button><span>+</span>Добавить файлы</button>
                </div>
            </div>
            <div class="Additional">
                <input id="need-logo" type="checkbox"/>
                <label for="need-logo">Требуется создание логотипа</label>
            </div>
            <div class="Commentary">
                <span>Комментарии к логотипу</span>
                <textarea
                    placeholder="Должен символизировать основной товар магазина - компьютерные комплектующие. В современном стиле"></textarea>
            </div>
            <div class="Commentary">
                <span>Комментарии к дизайну</span>
                <textarea
                    placeholder="Flat-дизайн, светлый, контрастный. Цветовая схема - сочетание голубого и красных цветов."></textarea>
            </div>
            <button class="Go-Next">Отправить</button>
        </div>
    </form>
</section>